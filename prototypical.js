var Model = Object.create(null); // you didn't actually specify any properties...

var User = Object.create(Model, {
  name: { // same syntax as defineProperty. extra newlines for clarity.
    get: function() {
      // getters/setters like this are unnecessary, but whatever, I'll just assume it's placeholder code.
      return this._name;
    }
  }
});
