var inherits = require('utils').inherits
var Model = require('some-orm').Model

function User () {
  Model.call(this)
}

inherits(User, Model)

Model.prototype.getName () {
  return this._name
}